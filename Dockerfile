FROM python:3.9-slim-buster 
WORKDIR /flask
COPY . .
RUN pip3 install -r /flask/source_code/requirements.txt
EXPOSE 8181
CMD [ "python", "/flask/source_code/server.py" ]